#import "RealmFlutterPlugin.h"
#import <realm_flutter/realm_flutter-Swift.h>

@implementation RealmFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftRealmFlutterPlugin registerWithRegistrar:registrar];
}
@end
